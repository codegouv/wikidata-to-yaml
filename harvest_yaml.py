#! /usr/bin/env python3


# wikidata-to-yaml -- Convert Wikidata data from to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2016 Etalab
# https://git.framasoft.org/codegouv/wikidata-to-yaml
#
# wikidata-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# wikidata-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import collections
import json
import os
import shutil
import sys
import urllib.parse
import urllib.request

from slugify import slugify
import yaml


# YAML configuration


class folded_str(str):
    pass


class literal_str(str):
    pass


def dict_constructor(loader, node):
    return collections.OrderedDict(loader.construct_pairs(node))


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.items()))


yaml.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, dict_constructor)

yaml.add_representer(folded_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_dir', help = 'path of target directory for harvested HTML pages')
    args = parser.parse_args()

    if os.path.exists(args.yaml_dir):
        for filename in os.listdir(args.yaml_dir):
            if filename.startswith('.'):
                continue
            if len(filename) != 1:
                continue
            shutil.rmtree(os.path.join(args.yaml_dir, filename))
    else:
        os.makedirs(args.yaml_dir)

    tool_by_slug = {}
    for sparql_query_filename in ('query1.sparql', 'query2.sparql'):
        print(sparql_query_filename)
        with open(sparql_query_filename, encoding = 'utf-8') as sparql_query_file:
            sparql_query = sparql_query_file.read()
        # sparql_query = '{}\nLIMIT 1000\n'.format(sparql_query)
        query = urllib.parse.urlencode(dict(query = sparql_query))
        request = urllib.request.Request(
            headers = {
                'Accept': 'application/json',
                },
            url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?{}'.format(query),
            )
        try:
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as e:
            print('HTTPError:\n{}'.format(e.read().decode('utf-8')))
            raise
        body_bytes = response.read()
        with open('body.txt', 'wb') as f:
            f.write(body_bytes)
        body = json.loads(body_bytes.decode('utf-8'))
        bindings = body['results']['bindings']
        for binding in bindings:
            label = binding.get('label_en') or binding.get('label_fr') or binding.get('label_es')
            if label is None:
                continue
            slug = slugify(label['value'])
            tool = tool_by_slug.setdefault(slug, collections.OrderedDict())
            for key, value in binding.items():
                items = tool.setdefault(slugify(key).replace('-', '_'), [])
                for item in items:
                    if item == value:
                        break
                else:
                    items.append(value)

    for slug, tool in tool_by_slug.items():
        dir = os.path.join(args.yaml_dir, slug[0])
        if not os.path.exists(dir):
            os.makedirs(dir)
        yaml_path = os.path.join(dir, '{}.yaml'.format(slug))
        with open(yaml_path, 'w', encoding = 'utf-8') as yaml_file:
            yaml.dump(tool, yaml_file, allow_unicode = True, default_flow_style = False, indent = 2, width = 120)

    return 0


if __name__ == '__main__':
    sys.exit(main())
